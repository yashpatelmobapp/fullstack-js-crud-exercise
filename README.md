# Plexxis Interview Exercise
## Additional Dependencies 
* npm install mysql
* npm install react-table
* npm install react-bootstrap

## Getting Started
* Install all npm Dependencies
* Host MySQL Database on localhost
* Import plexxisdb from project files to phpmyadmin (from database folder)
       - host : 'localhost'
       - user: 'root'
       - database: 'plexxisdb'
* npm start

## Goal & Where I focussed your effort
My goal was to complete all exercise requirement including bonus one. I have focused more on frontend part and tried to keep it simple but attractive UI. Built REST API endpoints to perform CURD operations on the MySQL database.

### API end points
* /apidemo - Returns employees list
* /apidemo/add - Add new employee to existing database
* /apidemo/remove - Remove selected employee from database

### List of components
* NewEmployeeForm 
       - Collection of input fields and button. The user can enter new employee new entries and submit a form to API which will enter new data to the database.
       - ![alt text](https://bitbucket.org/yashpatelmobapp/fullstack-js-crud-exercise/raw/e6a9929d1c28bbc8c723abfa17da09f8b3b530fe/showcase/newemp.jpg =500x)
* Card
       - Collection of labels and buttons. It shows all details of employees including a profile picture. The user can remove a specific employee by clicking the remove button.
       - ![alt text](https://bitbucket.org/yashpatelmobapp/fullstack-js-crud-exercise/raw/e6a9929d1c28bbc8c723abfa17da09f8b3b530fe/showcase/card.jpg)

* CardList
       - Collection of cards for different employees. Update list whenever a new employee gets added or removed from the database.
       - ![alt text](https://bitbucket.org/yashpatelmobapp/fullstack-js-crud-exercise/raw/e6a9929d1c28bbc8c723abfa17da09f8b3b530fe/showcase/cardlist.jpg)

### Things I could do better
* I could use redux to maintain center truth for all components and it would make easy to update employee details. I didn't use redux and it causes different state between parent-child components which is hard to manage. I am working on it. I will update soon.

### Demo
![alt text](https://bitbucket.org/yashpatelmobapp/fullstack-js-crud-exercise/raw/9a73f9f7e5237cb786ce5f3427e84804386a68bc/showcase/Plexxisdemo.gif)

