import React from 'react';
import logo from './logo-emp.svg'
import { Modal,Popover, Tooltip, Button, OverlayTrigger } from 'react-bootstrap';

// Import React Table
import "react-table/react-table.css";

class App extends React.Component {
  state = {
    employees: []
  }
  
  componentWillMount = () => {
    this.getEmp();
  }

  getEmp = _ => {
    fetch('http://localhost:8080/apidemo')
      .then(response => response.json())
      .then(response => this.setState({ employees: response.data }))
      .then(({ data }) => {
        console.log(data)
      })
      .catch(err => console.error(err))
  }

  addEmp = (event)=> {
    event.preventDefault();
    const { employees } = this.state;
    fetch(`http://localhost:8080/apidemo/add?name=${employees.name}&code=${employees.code}&profession=${employees.profession}&color=${employees.color}&city=${employees.city}&branch=${employees.branch}&assigned=${employees.assigned}`)
      .then(this.getEmp)
      .catch(err => console.error(err))
  }

  removeEmp = (id) =>{
    console.log(id);
    fetch(`http://localhost:8080/apidemo/remove?id=${id}`)
      .then(this.getEmp)
      .catch(err => console.error(err))
  }

  deleteButtonClicked = (getId) => {
    this.removeEmp(getId);
  };

  render() {
    const {
      employees
    } = this.state;

    const textAlignCenter = {
      textAlign:'center',
      fontWeight:'bold'
    };

    console.log(this.state);

    return (
      <div className="App">
        <h1 style={textAlignCenter}>Plexxis Employees</h1>
        <NewEmployeeForm addEmp={this.addEmp} getEmp={this.getEmp}></NewEmployeeForm>
        <CardList employees={this.state.employees} deleteButton={this.deleteButtonClicked}></CardList>
      </div>
    );
  }
}

class NewEmployeeForm extends React.Component { 

  constructor(props) {
    super(props);

    this.state = {
      newEmp: {
        name: 'Default Name',
        code: 'Default Code',
        profession: 'Default Profession',
        color: 'Default Color',
        city: 'Default City',
        branch: 'Default Branch',
        assigned: 'false'
      }
    }
  }

  addEmp = (event)=> {
    event.preventDefault();
    const { newEmp } = this.state;
    fetch(`http://localhost:8080/apidemo/add?name=${newEmp.name}&code=${newEmp.code}&profession=${newEmp.profession}&color=${newEmp.color}&city=${newEmp.city}&branch=${newEmp.branch}&assigned=${newEmp.assigned}`)
      .then(this.props.getEmp)
      .catch(err => console.error(err))
  }


  render() { 

    const { newEmp } = this.state;

    return (
      <div className="container" >
        <form className="shadow p-4 mb-5 bg-white rounded" >
          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Name</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" id="inputEmail3" placeholder="Name" onChange={e => this.setState({ newEmp: { ...newEmp, name: e.target.value } })} required/>
            </div>
          </div>

          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Code</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" id="inputEmail3" placeholder="Code" onChange={e => this.setState({ newEmp: { ...newEmp, code: e.target.value } })} required/>
            </div>
          </div>


          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Profession</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" id="inputEmail3" placeholder="Profession" onChange={e => this.setState({ newEmp: { ...newEmp, profession: e.target.value } })} required/>
            </div>
          </div>


          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Color</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" id="inputEmail3" placeholder="Color" onChange={e => this.setState({ newEmp: { ...newEmp, color: e.target.value } })} required/>
            </div>
          </div>

          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">City</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" id="inputEmail3" placeholder="City" onChange={e => this.setState({ newEmp: { ...newEmp, city: e.target.value } })} required/>
            </div>
          </div>

          
          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Branch</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" id="inputEmail3" placeholder="Branch" onChange={e => this.setState({ newEmp: { ...newEmp, branch: e.target.value } })} required/>
            </div>
          </div>

              <fieldset className="form-group">
                <div className="row">
                  <legend className="col-form-label col-sm-2 pt-0">Assigned</legend>
                    <div className="col-sm-10">
                      <div className="form-check">
                        <input className="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="true" onChange={e => this.setState({ newEmp: { ...newEmp, assigned: e.target.value } })}/>
                          <label className="form-check-label" htmlFor="gridRadios1">
                            Yes
                          </label>
                        </div>
                        <div className="form-check">
                          <input className="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="false" onChange={e => this.setState({ newEmp: { ...newEmp, assigned: e.target.value } })} />
                            <label className="form-check-label" htmlFor="gridRadios2">
                              No
                            </label>
                        </div>   
                    </div>
                </div>
              </fieldset>
              <div className="form-group row">
                  <button type="submit" className="btn btn-success" onClick={this.addEmp} style={{ width:"100%" }}>
                      Submit
                  </button>
              </div>
        </form>
      </div>
    );
  }
}

class CardList extends React.Component {
  render() {
    return(
      this.props.employees.map(employee => (
        <Card key={employee.id} emp={employee} deleteButton={this.props.deleteButton}></Card>
      ))
    );
  }
}

class Card extends React.Component {
  
    constructor(props) {
      super(props);
      this.state = {};
    }
    
    componentWillMount = () => {
      Object.keys(this.props.emp).map(key => 
        {
          this.setState({
            [key]: this.props.emp[key]
          });
        }
      )
    }


    handleEditClick = () => {
      console.log("Edit Button is working");
    };

    handleDeleteClick = () => {
      this.props.deleteButton(this.state.id);
    };

    render() {

      const outerContainerStyle = {
        padding: '0px',
        marginTop: '20px',
      };

      const innerContainerStyle = {
        padding: '0px',
      };

      const tableContainerStyle = {
        padding: '0px',
        width:'80%'
      };

      const tableDataStyle = {
        width: '50%',
      };

      const buttonStyle = {
        width: '100%',
      };
  
      return(
        <div className="container" style={outerContainerStyle} >
          <div className="card" className="shadow p-3 mb-5 bg-white rounded">
              <div className="container" style={innerContainerStyle}>
                <h5 className="card-header">{this.state.name}</h5>
                <img src={logo} className="rounded float-left" alt="..." height="200px" width="200px" style={ {padding: "25px"} }/>
                <div className="card-body">
                  <table className="table table-sm" style={tableContainerStyle}>
                    <tbody>
                      <tr>
                        <td style={tableDataStyle}>ID</td>
                        <td>{this.state.id}</td>
                      </tr>
                      <tr>
                        <td>Name</td>
                        <td>{this.state.name}</td>
                      </tr>
                      <tr>
                        <td>Code</td>
                        <td>{this.state.code}</td>
                      </tr>
                      <tr>
                        <td>Profession</td>
                        <td>{this.state.profession}</td>
                      </tr>
                      <tr>
                        <td>Color</td>
                        <td>{this.state.color}</td>
                      </tr>
                      <tr>
                        <td>City</td>
                        <td>{this.state.city}</td>
                      </tr>
                      <tr>
                        <td>Branch</td>
                        <td>{this.state.branch}</td>
                      </tr>
                      <tr>
                        <td>Assigned</td>
                        <td>{this.state.assigned}</td>
                      </tr>
                      <tr>
                        <td><button href="#" className="btn btn-primary" style={buttonStyle} onClick={this.handleEditClick}>Edit</button></td>
                        <td><button href="#" className="btn btn-danger" style={buttonStyle} onClick={this.handleDeleteClick}>X Remove</button></td>
                      </tr>
                    </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
      );
    }
}

export default App;
