const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const app = express()
app.use(cors());
const SELECT_ALL_EMP = 'SELECT * FROM plexxisemp';

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'plexxisdb'
});

connection.connect(err => {
  if(err)
  {
      return err; 
  }
});

//Get All Plexxis Employees
app.get('/apidemo', (req, res) =>{
  connection.query(SELECT_ALL_EMP ,(err, results) => {
      if(err){
          return res.send(err)
      }
      else{
          return res.json({
              data: results
          })
      }
  });
});

//Add New Plexxis Employee
app.get('/apidemo/add', (req, res) => {
  const {name, code, profession, color, city, branch, assigned } = req.query;
  // const INSERT_NEW_USERS = `INSERT INTO users (id, firstname, lastname, email) VALUES (NULL, '${firstname}', '${lastname}','${email}')`
  const INSERT_NEW_EMP = `INSERT INTO plexxisemp (id, name, code, profession, color, city, branch, assigned) VALUES (NULL, '${name}', '${code}','${profession}','${color}','${city}','${branch}','${assigned}')`

  connection.query(INSERT_NEW_EMP, (err, results) => {
      if(err){
          return res.send(err)
      }
      else{
          return res.send('Added New Employee')
      }
  });
});


//Remove Plexxis Employee
app.get('/apidemo/remove', (req, res) => {
  const {id} = req.query;
  const REMOVE_EMP = `DELETE FROM plexxisemp WHERE plexxisemp.id = ${id}`
  connection.query(REMOVE_EMP, (err, results) => {
      if(err){
          return res.send(err)
      }
      else{
          return res.send('Removed Employee')
      }
  });
});

// cors(corsOptions)

app.get('/api/employees',(req, res, next) => {
  console.log('/api/employees');
  res.setHeader('Content-Type', 'application/json');
  res.status(200);
  res.send(JSON.stringify(employees, null, 2));
})

app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'))